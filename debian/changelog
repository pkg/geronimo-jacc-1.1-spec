geronimo-jacc-1.1-spec (1.0.3-1+apertis2) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 21:18:37 +0530

geronimo-jacc-1.1-spec (1.0.3-1+apertis1) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:14 +0530

geronimo-jacc-1.1-spec (1.0.3-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 16:37:41 +0000

geronimo-jacc-1.1-spec (1.0.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Generate the OSGi metadata
  * Depend on libservlet-api-java instead of libservlet3.1-java
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13
  * Removed debian/orig-tar.sh
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 21 Jan 2021 16:10:04 +0100

geronimo-jacc-1.1-spec (1.0.1-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 11:39:47 +0000

geronimo-jacc-1.1-spec (1.0.1-2) unstable; urgency=medium

  * Team upload.
  * Build with maven-debian-helper instead of ant
  * Transition to the Servlet API 3.1 (Closes: #801030)
  * Package adopted by the Java Team
  * Standards-Version updated to 3.9.8 (no changes)
  * Removed the deprecated DM-Upload-Allowed field
  * Switch to debhelper level 9
  * Switch to source format 3.0 (quilt)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 30 May 2016 12:59:47 +0200

geronimo-jacc-1.1-spec (1.0.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Install POM file with maven-repo-helper. (Closes: #592238)

 -- Torsten Werner <twerner@debian.org>  Wed, 19 May 2010 23:01:16 +0200

geronimo-jacc-1.1-spec (1.0.1-1) unstable; urgency=low

  * Port package to pkg-java based largely on existing Ubuntu package

 -- Chris Grzegorczyk <grze@eucalyptus.com>  Wed, 16 Dec 2009 21:49:27 -0800

geronimo-jacc-1.1-spec (1.0.1-0ubuntu2) karmic; urgency=low

  * debian/build.xml, debian/rules: Enable test suite
  * debian/control, debian/rules: Adding junit and ant-optional as they are
    needed for the junit task used in tests.

 -- Thierry Carrez <thierry.carrez@ubuntu.com>  Wed, 26 Aug 2009 12:34:44 +0200

geronimo-jacc-1.1-spec (1.0.1-0ubuntu1) karmic; urgency=low

  * Initial release. New Eucalyptus dependency.

 -- Thierry Carrez <thierry.carrez@ubuntu.com>  Mon, 27 Jul 2009 11:19:09 +0200
